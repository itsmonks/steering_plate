from os import system
import numpy as np
import cv2
from math import atan2, degrees
from keypress import PressKey, ReleaseKey, D, A, W

def performKeyPress(angle):
    PressKey(W)
    if angle >= 250 and angle <= 290:
        ReleaseKey(A)
        ReleaseKey(D)
    elif angle >= 90 and angle < 270:
        ReleaseKey(A)
        PressKey(D)
    else:
        ReleaseKey(D)
        PressKey(A)


def getOrientation(pts, img, tip):
  # Construct a buffer used by the pca analysis
  sz = len(pts)
  data_pts = np.empty((sz, 2), dtype=np.float64)
  for i in range(data_pts.shape[0]):
    data_pts[i,0] = pts[i,0,0]
    data_pts[i,1] = pts[i,0,1]
 
  # Perform PCA analysis
  mean = np.empty((0))
  mean, __, __ = cv2.PCACompute2(data_pts, mean)
 
  # Store the center of the object
  cntr = (int(mean[0,0]), int(mean[0,1]))
 
  ## [visualization]
  # Draw the cntr
  cv2.circle(img, cntr, 3, (255, 0, 255), 2)
 
  angle = degrees(atan2(tip[1] - cntr[1], tip[0] - cntr[0]))
  return angle % 360

def preprocess(frame):
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frame_blur = cv2.GaussianBlur(frame_gray, (5, 5), 1)
    frame_canny = cv2.Canny(frame_blur, 50, 50)
    kernel = np.ones((3, 3))
    frame_dilate = cv2.dilate(frame_canny, kernel, iterations=2)
    frame_erode = cv2.erode(frame_dilate, kernel, iterations=1)
    return frame_erode

def find_tip(points, convex_hull):
    length = len(points)
    indices = np.setdiff1d(range(length), convex_hull)

    for i in range(2):
        j = indices[i] + 2
        if j > length - 1:
            j = length - j
        if np.all(points[j] == points[indices[i - 1] - 2]):
            return tuple(points[j])

def startCamera():
    system('cls')
    cap = cv2.VideoCapture(0)
    while(True):
        ret, frame = cap.read()
        processed_frame = preprocess(frame)
        contours, hierarchy = cv2.findContours(processed_frame, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        for cnt in contours:
            peri = cv2.arcLength(cnt, True)

            # filter all that don't have a permieter larger than 500 or one bigger than 900
            if peri < 500 or peri > 900:
              continue

            approx = cv2.approxPolyDP(cnt, 0.012 * peri, True)
            hull = cv2.convexHull(approx, returnPoints=False)
            sides = len(hull)

            if 6 > sides > 3 and sides + 2 == len(approx):
                arrow_tip = find_tip(approx[:,0,:], hull.squeeze())
                if arrow_tip:
                    cv2.drawContours(frame, [cnt], -1, (0, 255, 0), 3)
                    orientation = getOrientation(cnt, frame, arrow_tip)
                    print("Orientation: " + str(orientation) + " perimeter " + str(peri))
                    performKeyPress(orientation)
                    cv2.circle(frame, arrow_tip, 3, (0, 0, 255), cv2.FILLED)
                else:
                    ReleaseKey(W)

        cv2.imshow("Image", frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    startCamera()
